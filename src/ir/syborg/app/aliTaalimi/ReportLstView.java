package ir.syborg.app.aliTaalimi;

import java.util.ArrayList;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class ReportLstView extends Activity {

    public ArrayList<StructNote> notes = new ArrayList<StructNote>();
    public ArrayAdapter          adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_lstview);

        ListView lst_report = (ListView) findViewById(R.id.lst_report);

        adapter = new AdapterNote(notes);
        lst_report.setAdapter(adapter);

        Cursor cursor = G.database.rawQuery("select * from company", null);

        while (cursor.moveToNext()) {

            int company_id = cursor.getInt(cursor.getColumnIndex("company_id"));
            String company_name = cursor.getString(cursor.getColumnIndex("company_name"));
            String family = cursor.getString(cursor.getColumnIndex("family"));
            String phone = cursor.getString(cursor.getColumnIndex("phone"));
            String mobile = cursor.getString(cursor.getColumnIndex("mobile"));
            String address = cursor.getString(cursor.getColumnIndex("address"));

            StructNote note = new StructNote();
            note.company_id = company_id;
            note.company_name = company_name;
            note.family = family;
            note.phone = phone;
            note.mobile = mobile;
            note.address = address;

            notes.add(note);

        }
        adapter.notifyDataSetChanged();

    }

}
