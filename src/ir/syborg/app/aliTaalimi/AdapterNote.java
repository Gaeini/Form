package ir.syborg.app.aliTaalimi;

import java.util.ArrayList;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class AdapterNote extends ArrayAdapter<StructNote> {

    public AdapterNote(ArrayList<StructNote> array) {
        super(G.context, R.layout.adapter_note, array);

    }


    private static class ViewHolder {

        public TextView company_id;
        public TextView company_name;
        public TextView family;
        public TextView phone;
        public TextView mobile;
        public TextView address;


        public ViewHolder(View view) {

            company_id = (TextView) view.findViewById(R.id.company_id);
            company_name = (TextView) view.findViewById(R.id.company_name);
            family = (TextView) view.findViewById(R.id.family);
            phone = (TextView) view.findViewById(R.id.phone);
            mobile = (TextView) view.findViewById(R.id.mobile);
            address = (TextView) view.findViewById(R.id.address);

        }


        public void fill(ArrayAdapter<StructNote> adpter, StructNote item, int position) {

            company_id.setText(""+item.company_id);
            company_name.setText(item.company_name);
            family.setText(item.family);
            phone.setText(item.phone);
            mobile.setText(item.mobile);
            address.setText(item.address);

        }

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        StructNote item = getItem(position);
        if (convertView == null) {
            convertView = G.inflater.inflate(R.layout.adapter_note, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.fill(this, item, position);
        return convertView;
    }
}
