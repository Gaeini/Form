package ir.syborg.app.aliTaalimi;

import java.io.File;
import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.view.LayoutInflater;


public class G extends Application {

    public static LayoutInflater inflater;
    public static Context        context;
    public static SQLiteDatabase database;
    public static final String   DIR_SDCARD   = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String   DIR_DATABASE = DIR_SDCARD + "/AliTaalimi_DataBase/";


    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        new File(DIR_DATABASE).mkdirs();
        database = SQLiteDatabase.openOrCreateDatabase(G.DIR_DATABASE + "/mainTable.sqlite", null);
        database.execSQL("CREATE  TABLE  IF NOT EXISTS company (" +
                "company_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE ," +
                " company_name TEXT," +
                " family TEXT," +
                " phone TEXT," +
                " mobile TEXT," +
                " address TEXT)"
                );

    }
}
