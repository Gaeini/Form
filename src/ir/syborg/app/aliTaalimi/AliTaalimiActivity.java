package ir.syborg.app.aliTaalimi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AliTaalimiActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final EditText edt_Company_name = (EditText) findViewById(R.id.edt_Company_name);
        final EditText edt_family = (EditText) findViewById(R.id.edt_family);
        final EditText edt_phone = (EditText) findViewById(R.id.edt_phone);
        final EditText edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        final EditText edt_address = (EditText) findViewById(R.id.edt_address);
        Button btn_insert = (Button) findViewById(R.id.btn_insert);
        Button btn_report = (Button) findViewById(R.id.btn_report);

        btn_insert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                G.database.execSQL("INSERT INTO company (" +
                        "company_name,family,phone,mobile,address)" +
                        " VALUES (" +
                        "'" + edt_Company_name.getText() + "'," +
                        "'" + edt_family.getText() + "'," +
                        "'" + edt_phone.getText() + "'," +
                        "'" + edt_mobile.getText() + "'," +
                        "'" + edt_address.getText() + "')");
                String space = "";
                edt_Company_name.setText(space);
                edt_family.setText(space);
                edt_phone.setText(space);
                edt_mobile.setText(space);
                edt_address.setText(space);

                Toast.makeText(G.context, "اطلاعات با موفقیت ثبت شد", Toast.LENGTH_SHORT).show();
            }
        });

        btn_report.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent intent = new Intent(AliTaalimiActivity.this, ReportLstView.class);
                AliTaalimiActivity.this.startActivity(intent);

            }
        });

    }
}